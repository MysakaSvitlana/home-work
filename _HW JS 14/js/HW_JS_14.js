// ---------hide gallery--------------
$(document).ready(function () {
  $("#button-hide").click(function () {
    $(".galery").slideToggle(2500);
  });

// -------------- button --------------
  const goUp = $(".go-up");
  $(document).on("scroll", function () {
    if ($(this).scrollTop() >= $(window).height()) {
      goUp.fadeIn(300);
    } else {
      goUp.fadeOut(500);
    }
    goUp.on("click", function () {
      $("html").animate({ scrollTop: 0 }, 1000);
    });
  });

// -------------- scroll to title --------------
$(document).ready(function(){  
  $("a").on('click', function (event) { 
    if (this.hash !== "") { 
      event.preventDefault();
      var hash = this.hash;// Store hash
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 600, function(){
        window.location.hash = hash;
       });
      } 
    });
  });
});
  const navScrollBar = document.querySelector(".nav-scroll");
  
  navScrollBar.addEventListener("click", (event) => {
      event.preventDefault();
      const tabs = navScrollBar.querySelectorAll(".item-scroll");
      tabs.forEach((eachOneTabs) => {
      eachOneTabs.classList.remove("active");
      });
      event.target.classList.add("active");
      });
