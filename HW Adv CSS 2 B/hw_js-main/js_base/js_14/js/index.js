$(document).ready(function () {
	const backToTopBtn = $('.back-to-top-btn');

	$(window).on('scroll', () => {
		if ($(this).scrollTop() >= $(window).height()) {
			backToTopBtn.fadeIn(500);
		} else {
			backToTopBtn.fadeOut(500);
		}
	});

	backToTopBtn.on('click', () => {
		$('html').animate(
			{
				scrollTop: 0,
			},
			800
		);
	});

	$('.header-menu .header-menu-link').on('click', function (e) {
		if (this.hash) {
			e.preventDefault();
			const hash = this.hash;
			$('html, body').animate(
				{
					scrollTop: $(hash).offset().top,
				},
				800
			);
		}
	});

	$('.hide-show-btn').on('click', () => {
		$('.section-hot-news').slideToggle('slow');
	});
});
