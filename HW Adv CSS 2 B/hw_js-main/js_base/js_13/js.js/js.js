// Взять любое готовое домашнее задание по HTML/CSS.
// Добавить на макете кнопку "Сменить тему".
// При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
// Выбранная тема должна сохраняться и после перезагрузки страницы

const btnUp = document.querySelector('.button-update');
const findLink = document.getElementById('link');

const loadPage = function (event) {
    if (!localStorage.theme){
        localStorage.setItem('theme', 'oldPage');
        return;
    }

    if(localStorage.theme === 'oldPage'){
    findLink.href = './css/css_two.css';
    }else {
    findLink.href = './css/css.css';
    }
    
};

const turnOn = function () {
    if(localStorage.theme === 'oldPage'){
        localStorage.theme = 'newPage';
       findLink.href = './css/css_two.css';
    }else{
        localStorage.theme = 'oldPage'
        findLink.href = './css/css.css';
    }
};

document.addEventListener('DOMContentLoaded', loadPage);
btnUp.addEventListener('click', turnOn);









