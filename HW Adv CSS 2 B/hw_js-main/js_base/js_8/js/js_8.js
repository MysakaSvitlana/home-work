// Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:
// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. 
// Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: 
// Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). 
//Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. 
// Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, 
// под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
// В папке img лежат примеры реализации поля ввода и создающегося span.



let createinput = document.createElement('input') //cоздаём input
    createinput.setAttribute('value', 'price') // предназначен для добавления в элемент нового атрибута с указанным значением.
    document.body.append(createinput) // выводим в html
     
    let creatspan = document.createElement('span') 
    creatspan.innerHTML = '' //Свойство innerHTML позволяет получить HTML-содержимое элемента в виде строки.
    createinput.after(creatspan) 
 
    createinput.addEventListener('focus', () => { 
        createinput.style.cssText = 'outline: none; border: 3px solid green' 
    }) //обработчик с фокусом
 
    createinput.addEventListener('blur', () => { 
        let value = +createinput.value 
        let creatspanOne = document.createElement('span') 
        creatspanOne.style.cssText = 'display: block' 
        creatspanOne.innerHTML = `Текущая price: ${value}`
        let button = document.createElement('button') 
        button.innerHTML = ' x' 
        creatspanOne.append(button) 
        createinput.before(creatspanOne) 
        
        // if (createinput.value <= 0 || createinput.value === '' || Number.isNaN(parseFloat(inputPrice.value)){
        //     createinput.style.background = ''
        //     createinput.style.border = '1px solid red'
        // }
}) //Событие blur вызывается когда элемент теряет фокус. Главное отличие между этим событием и  focusout только в том что у последнего есть фаза всплытия.

 
    document.body.onclick = function(e) { 
        if (e.target.tagName == 'BUTTON') { 
            e.target.closest('span').remove() 
        } 
 
    }
//Свойство event.target содержит элемент, на котором сработало событие.
