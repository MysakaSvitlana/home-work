// В файле index.html лежит разметка для двух полей ввода пароля.
// По нажатию на иконку рядом с конкретным полем - должны отображаться символы, 
//которые ввел пользователь, иконка меняет свой внешний вид.
// В комментариях под иконкой - иконка другая, 
//именно она должна отображаться вместо текущей.
// Когда пароля не видно - иконка поля должна выглядеть,
// как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) 
//с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета 
// Нужно ввести одинаковые значения

// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.

const inputConfirmPassword = document.querySelector('.input-confirm-password');
const inputPass = document.querySelector('#password');
const confirmPass = document.querySelector('#confirm');
const btn = document.querySelector('.btn');
const message = document.createElement('p');

const showOrHidePassword = (e) => {
	const target = e.target;

	if (target.classList.contains('icon-password')) {
		target.classList.toggle('fa-eye-slash');
		const input = target.parentElement.querySelector('.input-password');
		if (target.classList.contains('fa-eye-slash')) {
			input.type = 'text';
		} else {
			input.type = 'password';
		}
	}
};

const comparePassword = (e) => {
	e.preventDefault();
	if (!inputPass.value.trim() && !confirmPass.value.trim()) {
		return;
	}

	if (inputPass.value.trim() !== confirmPass.value.trim()) {
		message.innerHTML = 'Нужно ввести одинаковые значения';
		confirmPass.after(message);
		message.classList.add('error-message');
	} else {
		alert('You are welcome');
        message.remove();
	}
};

inputConfirmPassword.addEventListener('click', showOrHidePassword);
btn.addEventListener('click', comparePassword);
