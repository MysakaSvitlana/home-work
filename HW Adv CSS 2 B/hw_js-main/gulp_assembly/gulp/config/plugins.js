import replace from "gulp-replace"; // поиск и замена картинок

//создаём объект в котором будет экспортировать и в него будем собирать общии плагины
import plumber from "gulp-plumber"; //обработка ошибок
import notify from "gulp-notify"; // сообщения об ошибке
import browsersync from "browser-sync"// локал сервер (//ввывод html файлов в браузер, а именно запуск локального сервера, открыть браущер, открыть индексную стр и при любыз ихменнений этот браузер обновляить)

export const plugins = {
    replace: replace,
    plumber: plumber, //помогает нам обработать возникающие ошбки при работе с тем или иным файлом 
    notify: notify, //плагин который будет выводить сообщения о тех же ошибках  npm i -D gulp-plumber gulp-notify
    browsersync: browsersync
}


