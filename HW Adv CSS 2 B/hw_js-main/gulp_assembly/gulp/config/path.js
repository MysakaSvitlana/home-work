import * as nodePath from 'path';
const rootFolder = nodePath.basename(nodePath.resolve());


const buildFolder = `./dist`; //также можно использывать название текущего проекта rootFolder
const srcFolder = `./src`;

 export const path = {
    build: {
        css: `${buildFolder}/css`,// выгркжыем результат scss
        html:`${buildFolder}/`,
        files:`${buildFolder}/files/`
    },
    src: {
        scss:`${srcFolder}/scss/style.scss`,
        html:`${srcFolder}/*.pug`,
        files: `${srcFolder}/files/**/*.*`,
    },
    watch:{
        scss:`${srcFolder}/scss/**/*.scss`,// наблюдаем обсолютно всеми файлами sсss которые нахолятся в папке исходники 
        html:`${srcFolder}/**/*.pug`,
        files: `${srcFolder}/files/**/*.*`
    },
    clean:buildFolder,
    buildFolder: buildFolder,
    srcFolder: srcFolder,
    rootFolder:rootFolder,
    ftp: ``
}

