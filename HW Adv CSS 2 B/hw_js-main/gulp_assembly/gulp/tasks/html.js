import fileinclude from "gulp-file-include";//npm i -D gulp-file-include
import webpHtmlNosvg from "gulp-webp-html-nosvg";//npm i -D gulp-webp-html-nosvg плагин чтобы обрабатывались отличные картинки от svg, а не все вподряд
import versionNumber from "gulp-version-number";// плагин позволяет избежать не приятных ситуаций  с кэширыванием это когда вы сделали кучк правок
//добавлять файлам  и стилей к js фалам определёный ключ который не позволит кешировать их в браузере 
import pug from "gulp-pug" //npm i -D gulp-pug

export const html = () => {
    return app.gulp.src(app.path.src.html)
    .pipe(app.plugins.plumber//обращаймся к плагину пламбер внутри него обратимся к плагину нотифай и при возникнование ошибок будет выводится сообщение 
        (app.plugins.notify.onError({
            title:"HTML",
            message:"Errore: <%= error.message %"
        }))
    )
    // .pipe(fileinclude())//собирает файл html из частей (коментируем или удаляем и вместо него добавляем  pug)
    .pipe(pug({
        //сжатия html файла 
        pretty:true,
        //показывает в терминале какой файл отработан
        verbose:true
    }))
    .pipe(app.plugins.replace(/@img\//g, 'img/')) //ищем все вхождения @img и меняем на нужные. Вместо @img, мы будем получать img
    .pipe(webpHtmlNosvg())//вызываем плагин
    .pipe(
        versionNumber({
            'value': '%DT%',
            'append': {
                'key': '_v',
                'cover': 0,
                'to': [
                    'css',
                    'js',
                ]
            },
            'output': {
                'file': 'gulp/version.json'
            }
        })  
        )//опдключается текущая дата и время за счёт этого обновляется кэш
    .pipe(app.gulp.dest(app.path.build.html))
    .pipe(app.plugins.browsersync.stream()); //обновляет html 
}


//обрабатываем подключения картинок таким оброзам чтобы мы могли подключать формат vp автоматически. 
//Формат изображения vp. Он позваляет сохранять качество изображения при значительно меньшем размере создавать автоматически из обычных картинок в эти картинки

