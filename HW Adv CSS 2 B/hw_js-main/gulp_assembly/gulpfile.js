//основной модуль 
import gulp from "gulp";
//импорт путей
import { path } from "./gulp/config/path.js";

//импорт общих плагинов 
import { plugins } from "./gulp/config/plugins.js"

//передаем значение в глобальную переменную

global.app = {
    path: path,
    gulp:gulp,
    plugins: plugins
}
//импорт задач (подключения задачь которые созхдали в tasks)

import {copy} from "./gulp/tasks/copy.js";
import {reset} from "./gulp/tasks/reset.js";
import {html} from "./gulp/tasks/html.js";
import {server} from "./gulp/tasks/server.js";
import {scss} from "./gulp/tasks/scss.js";

//наблюдатель за изменениями в файлах
function watcher(){
    gulp.watch(path.watch.files, copy);
    gulp.watch(path.watch.html, html);
    gulp.watch(path.watch.scss, scss);
}

const mainTask = gulp.parallel(copy, html, scss);

//посколько сценарий становится сложнеии ввыводим в конст
//построение сценариев выполнения задач
const dev = gulp.series(reset, mainTask, gulp.parallel(watcher, server));

//выполнение сценария по умолчанию

gulp.task('default', dev);