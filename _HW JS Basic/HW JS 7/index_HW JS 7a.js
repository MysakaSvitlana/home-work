let time;
function timer() {
  time = seconds.innerText;
  if (time != 0) {
    seconds.innerText--;
  } else {
    alert(`Список очищен. Нужно было сфоткать или записать`);
    clearInterval(interval);
  }
}
const interval = setInterval(timer, 1000);

const outPut = (array, parent = document.body) => {
  const parentElem = document.createElement("ul");
  const newChildElems = array.map((item) => `<li> ${item}</li>`);
  parentElem.insertAdjacentHTML("beforeend", newChildElems.join(" "));
  parent.append(parentElem);

  setTimeout(() => parentElem.remove(), 3000);
};

outPut(["Kyiv", "Kharkiv", "Lviv", "Rivne"]);

// function creatingList(array, parent = document.body) {
//   const ul = document.createElement(`ul`);
//   array.forEach((e) => {
//     const li = document.createElement(`li`);
//     if (Array.isArray(e)) {
//       creatingList(e, li);
//     } else {
//       li.innerText = e;
//     }
//     ul.append(li);
//   });
//   parent.append(ul);
//   setTimeout(() => ul.remove(), 3000);
// }
// creatingList([
//   "Kharkiv",
//   "Kiev",
//   ["Borispol", "Irpin", "Zhytomyr"],
//   "Odessa",
//   "Lviv",
//   "Dnieper",
// ]);
