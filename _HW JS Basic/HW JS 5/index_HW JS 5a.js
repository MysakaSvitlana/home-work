/*
Що таке екранування, для чого воно протрібне?
В JS є набір спеціальних символів, що використовуються в синтаксисі коду.
 Коли їх потрібно використовувати за прямим призначенням (як в друці),
 для зчитування їх, як звичайних, потрібно синтаксично 
 відмітити, що в даному випадку символ використовується, 
 як звичайний, цей процес і називається екрануванням.
*/

function createNewUser() {
  return {
    name: prompt("Enter your name"),
    lastName: prompt("Enter your surname"),
    birthday: prompt("How old are you? (dd.mm.yyyy)"),
    getLogin: function () {
      return this.name[0].toLowerCase() + this.lastName.toLowerCase();
    },
    getAge: function () {
      let dateToday = new Date();
      let year = +this.birthday.slice(6);
      let month = +this.birthday.slice(3, 5);
      month--;
      let day = +this.birthday.slice(0, 2);
      let age = dateToday.getFullYear() - year;
      if (
        month > dateToday.getMonth() ||
        (month === dateToday.getMonth && day > dateToday.getDay())
      ) {
        age--;
      }
      return age;
    },
    getPassword: function () {
      return (
        this.name[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(-4)
      );
    },
  };
}
const user = createNewUser();
console.log(user);
console.log(user.birthday);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
