let setArr = ["12", "word", 99, null, true, "number", 8, "boolean", false];
function filterBy(arr, type) {
  return arr.filter((item) => {
    if (item === null) {
      return type !== "null";
    } else {
      return typeof item !== type;
    }
  });
}

console.log(filterBy(setArr, "string"));
