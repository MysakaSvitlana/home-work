const buttonChange = document.querySelector(".change");
const linkStyle = document.getElementById("stylesheet");

const pageLoad = function (e) {
  if (!localStorage.theme) {
    localStorage.setItem("theme", "blue");
    return;
  }

  if (localStorage.theme === "blue") {
    linkStyle.href = "./css/style_13.css";
  } else {
    linkStyle.href = "./css/style_13 matt.css";
  }
};

const changeTheme = function () {
  if (localStorage.theme === "blue") {
    localStorage.theme = "matt";
    linkStyle.href = "./css/style_13 matt.css";
  } else {
    localStorage.theme = "blue";
    linkStyle.href = "./css/style_13.css";
  }
};

document.addEventListener("DOMContentLoaded", pageLoad);
buttonChange.addEventListener("click", changeTheme);
