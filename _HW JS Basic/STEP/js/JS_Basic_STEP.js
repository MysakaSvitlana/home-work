const tabs = document.querySelector(".tabs-items");
const tabsContent = document.querySelectorAll(".service-galery");

tabs.addEventListener("click", (event) => {
  const clickedLi = tabs.querySelectorAll(".tab-sub-item");
  clickedLi.forEach((eachoneTabTitle) => {
    eachoneTabTitle.classList.remove("active");
  });
  event.target.classList.add("active");
  let newTabAttr = event.target.getAttribute("data-item");

  showDescription(newTabAttr);
});


const showDescription = (attr) => {
  tabsContent.forEach((tabContent) => {
    if (tabContent.getAttribute("data-item") === attr) {
      tabContent.classList.remove("hidden");
    } else {
      tabContent.classList.add("hidden");
    }
  });
};



const btnLoadMorePhoto = document.querySelector(".load-more-btn");
const photoCategories = document.querySelectorAll(".img-gallery-item");

btnLoadMorePhoto.addEventListener("click", function () {
    photoCategories.forEach((element)=>{
    element.classList.remove("hidden")
  })
  setTimeout(() => {
    btnLoadMorePhoto.remove()
    }, 3000); 
})

const thirdBoot = document.querySelector("#thirdBoot");

thirdBoot.addEventListener("click", function (event) {
  photoCategories.forEach((element) => {
    element.classList.add("additional-hidden");
    if (event.target.dataset.content === element.dataset.content || event.target.dataset.content === "all") {
      element.classList.remove("additional-hidden");
    }
  })
})



//  --------slick-slider-----------------


// $(".slider-description").slick("slickGoTo", `${currentSlideIndex}`);

// $(".")



// 1. getcurrentslide = currentslide(small)
// 2. select slider with description ($(slider-description)).slick.(gotoSlide(getcurrentslide))
  $(".slider-small").slick({
    slidesToShow:4,
    slidesToScroll:1,
    centerPadding: "30",
    centerMode: true,
    asNavFor: '.slider-big',
    focusOnSelect: true,
    prevArrow: $(".arrow-handmade-prev"),
    nextArrow: $(".arrow-handmade-next"),
  });


  $(".slider-big").slick({
    arrows: false,
    slidesToShow:1,
    slidesToScroll:1,
    speed: 500,
    easing: 'ease',
    fade: true,
    asNavFor:'.slider-small'
  });


  $(".slider-description").slick({
    arrows: false,
    slidesToShow: 1,
    fade: true,
    slidesToScroll: 1,
  });


let currentSlideIndex = $(".slider-small").slick("slickCurrentSlide");

$('.slider-small').on('beforeChange', function (event, slick, currentSlide) {
   const currentElement = $(slick.$slides.get(currentSlide));
  currentElement[0].classList.remove("moveTop");
});



$(".slider-small").on('afterChange', function (event, slick) {
  currentSlideIndex = $(".slider-small").slick("slickCurrentSlide");
  $(".slider-description").slick("slickGoTo", `${currentSlideIndex}`);
  $(slick.$slides.get(currentSlideIndex));
  const currentElement = $(slick.$slides.get(currentSlideIndex));
  currentElement[0].classList.add("moveTop");
});


$(".slider-small").on('afterChange', function () {
  let slickElement = $(".slider-small").slick("getSlick");
  const currentSlideDom = $(slickElement.$slides.get(currentSlideIndex));
});