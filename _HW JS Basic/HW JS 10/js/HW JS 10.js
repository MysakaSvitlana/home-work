const form = document.querySelector(".password-form");
const message = document.createElement("span");

const toggleShowingPassword = (e) => {
  const input = e.target.parentElement.querySelector(".field-pass");
  if (e.target.classList.contains("fa-eye-slash")) {
    e.target.classList.remove("fa-eye-slash");
    e.target.classList.add("fa-eye");
    input.type = "text";
  } else {
    e.target.classList.remove("fa-eye");
    e.target.classList.add("fa-eye-slash");
    input.type = "password";
  }
};

const compareValue = () => {
  const inputPassword = document.querySelector(".password");
  const confirmPassword = document.querySelector(".confirm-password");

  if (!inputPassword.value && !confirmPassword.value) {
    return;
  }

  if (inputPassword.value === confirmPassword.value) {
    alert("You are welcome");
    message.remove();
  } else {
    message.textContent = "Password must match";
    confirmPassword.after(message);
    message.style.color = "red";
  }
};

form.addEventListener("click", (e) => {
  e.preventDefault();

  if (e.target.classList.contains("icon-password")) {
    toggleShowingPassword(e);
    return;
  }

  if (e.target.classList.contains("btn")) {
    compareValue();
    return;
  }
});
