const tabs = document.querySelector(".tabs");
//console.log(tabs);
const tabsContent = document.querySelectorAll(".item");
//console.log(tabsContent);

tabs.addEventListener("click", (event) => {
  const clickedLi = tabs.querySelectorAll(".tabs-title");
  clickedLi.forEach((eachoneTabTitle) => {
    eachoneTabTitle.classList.remove("active");
  });
  event.target.classList.add("active");
  let newTabAttr = event.target.getAttribute("data-item");

  showDescription(newTabAttr);
});

const showDescription = (attr) => {
  tabsContent.forEach((tabContent) => {
    if (tabContent.getAttribute("data-item") === attr) {
      tabContent.classList.remove("hiding");
    } else {
      tabContent.classList.add("hiding");
    }
  });
};
