// Создать поле для ввода цены с валидацией.

// Технические требования:
// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть
// выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка
// с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
// В папке img лежат примеры реализации поля ввода и создающегося span.

// let createInput = document.createElement("input");
// createInput.setAttribute("value", "price :");
// document.body.append(createInput);
// document.getElementsByTagName("input").onkeypress = function (event) {
//   if (event.keyCode < 48 || event.keyCode > 57) {
//     return false;
//   }
// };
// createInput.addEventListener("focus", () => {
//   createInput.style.cssText =
//     "outline:none; border: 1px solid green; border-radius:10px;";
// });

// let createSpan = document.createElement("span");
// createSpan.innerHTML = "";
// createInput.after(createSpan);

// createInput.addEventListener("focusout", () => {
//   createInput.
// });

// createInput.addEventListener("blur", () => {
//   const value = +createInput.value;
//   value.cssText = "color:green;";
//   const createSpanOne = document.createElement("span");
//   createSpanOne.style.cssText = "display :block;";
//   createSpanOne.innerHTML = `Текущая цена:${value}`;
//   const button = document.createElement("button");
//   button.innerHTML = "X";
//   createSpanOne.append(button);
//   createInput.before(createSpanOne);
// });

// document.body.onclick = function (event){
//     if(event.target.tagName === "buttton")
// }
//With norm style

let inputPrice = document.createElement("input");
inputPrice.placeholder = "Price :";
inputPrice.style = "position: absolute; top: 35px; margin-left: 15px;";
document.body.append(inputPrice);
const errorMessage = document.createElement("span");
const list = document.createElement("ul");
list.classList.add("list");
// в css є стилі на input, коли поле вводу в фокусі, курсор стоїть в ньому
// ф-ція прибирає деякі стилі, щоб поле виглядало так як мені потрібно, без червоної рамки і зеленого кольору для
// на випадок якщо будуть введені не числа, то закрашувати їх в зелений не потрібно,
//і поки я їх друкую, вони теж не зелені
const focusFunc = (e) => {
  e.target.classList.remove("invalide-price", "input-text");
  e.target.classList.add("on-focus");
  errorMessage.remove();
  e.target.value = "";
};
// при знятті фокусу прибираємо ф-цією стиль з CSS і перевіряємо введене значення
const blurFunc = (e) => {
  e.target.classList.remove("on-focus");
  checkValue();
};

const checkValue = () => {
  // перевіряємо, якщо ціна меньша 0 або не число, чи є одиниці вимірювання поряд введені з цифрою,
  // якщо так видалаяє літери, і залишає значення числовим (може залишати числа з крапкою)
  if (
    inputPrice.value < 0 ||
    inputPrice.value === "" ||
    Number.isNaN(parseFloat(inputPrice.value))
  ) {
    inputPrice.classList.add("invalide-price"); // в наш основний input додаємо стиль
    // який застосовується при неправильно введених данних, якщо умова в попередньому if - true
    errorMessage.innerText = "Please enter correct price"; // в errorMessage (перемінну обявлену на початку) вводимо текст
    inputPrice.after(errorMessage); // додаємо цей напис після нашого основного input
  } else {
    inputPrice.classList.add("input-text");
    inputPrice.before(list);
    const li = document.createElement("li");
    li.classList.add("valide-price");
    const deleteBtn = createDelButton();
    li.innerText = `Current price : ${inputPrice.value}`;
    li.append(deleteBtn);
    list.append(li);
  }
};

const createDelButton = () => {
  const delBtn = document.createElement("button"); //cозд кнопку крестик
  delBtn.innerText = "X"; // розташовую в середені "Х"
  delBtn.style =
    "padding-left:4px; width: 20px; border: 2px solid rgb(129, 15, 15); border-radius: 25px";
  delBtn.addEventListener("click", (e) => {
    //слушаем событие, на батьківському елементі з тегом "li"
    e.target.closest("li").remove(); //по кліку на кнопку вона буде видаляти батьківський елемент
    //элемент span в котором написана `цена`
    inputPrice.value = ""; // присвоюємо значенню введеному в input пусту строку
    // щоб після закриття хрестиком зтерти введене значення
  });
  return delBtn;
};

inputPrice.addEventListener("focus", focusFunc);
inputPrice.addEventListener("blur", blurFunc);
