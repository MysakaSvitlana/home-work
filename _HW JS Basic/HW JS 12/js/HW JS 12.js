let showing = false;
let slideNum = 0;

let letsGo = setInterval(carousel, 2000);

const slides = document.querySelectorAll("#list .slide-item ");
const start = document.getElementById("start");
function startSlide() {
  if (!showing) {
    letsGo = setInterval(carousel, 2000);
    showing = true;
  }
}
start.addEventListener("click", startSlide);
const stop1 = document.getElementById("stop1");
function stopSlide() {
  clearInterval(letsGo);
  showing = false;
}
stop1.addEventListener("click", stopSlide);

function carousel() {
  slides[slideNum].className = "slide-item";
  slideNum = (slideNum + 1) % slides.length;
  slides[slideNum].className = "slide-item show";
}
margin: 0px 0px 0px 0px;