function keyPress(event) {
  const buttons = document.querySelectorAll(".button-list");
  for (let elem of buttons) {
    if (event.key) {
      elem.classList.remove("btn-checked");
    }
    if (elem.innerText.toLowerCase() == event.key.toLowerCase()) {
      elem.classList.add("btn-checked");
    }
  }

  console.log(event.key);
}
document.addEventListener("keypress", keyPress);
