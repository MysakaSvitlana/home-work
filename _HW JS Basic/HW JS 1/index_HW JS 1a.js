/*
1. var - застаріла, її не використовуємо, вона видима поза межами блоку, в якому об'явлена; до об'явлення
   має тип - undefined, він "не визначений", але вона (перемінна) є;
   const - об'являємо тільки, якщо впевнені, що перемінну не потрібно буде перезаписувати в цьому блоці;
   let - є видимою, в блоці, в якому об'явлена (завдяки цьому різна в кожній ітерації); переоб'являти її в цьому ж блоці не можна; не існує до об'явлення;

*/

let usersName;
let age;
do {
  usersName = prompt("What is your name?", usersName);
} while (usersName === "" || !isNaN(usersName));
do {
  age = +prompt("How old are you?");
} while (age === "" || isNaN(age));

if (age < 18) {
  alert("You are not allowed to visit this website");
} else if (age >= 18 && age <= 22) {
  if (confirm("Are you sure you want to continue?")) {
    alert(`Welcome, ${usersName}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else if (age > 22) {
  alert(`Welcome,  ${usersName}`);
}
