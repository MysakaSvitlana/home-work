/*Цикл призначений для багаторазового виконання дії або черги дій, 
  в не залежності від того знаємо їх кількість чи ні, для задавання 
  умов, за яких, цей цикл буде виконуватись, чи ні, чи повторюватись
*/
let number;
do {
  number = +prompt("Enter number", number);
} while (!Number.isInteger(number));

if (number < 5) {
  console.log("Sorry, no numbers");
} else {
  for (let i = 0; i <= number; i += 5) {
    console.log(i);
  }
}
let n;
let m;
do {
  m = +prompt("Enter smaller number");
} while (!Number.isInteger(m));
do {
  n = +prompt("Enter bigger number");
} while (!Number.isInteger(n));
nextPrime: for (let i = m; i <= n; i++) {
  for (let j = 2; j < i; j++) {
    if (i % j == 0) continue nextPrime;
  }
  console.log(i);
}
