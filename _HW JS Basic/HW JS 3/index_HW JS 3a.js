/*
1. Функції потрібні для того, щоб використати частину коду в різних місця документи, 
не дублюючи його весь.
2. Аргументи в функцію передаються, для того, щоб функція отримала очікувані значення
*/
let num1;
let num2;
do {
  num1 = +prompt("Enter first number", num1);
} while (parseInt.num1 === " " && !Number.isInteger(num1));
do {
  num2 = +prompt("Enter second number", num2);
} while (parseInt.num2 === " " && !Number.isInteger(num2));
let mathOperation = prompt("Enter mathematical operation");
let result;
function mathResult(num1, num2, mathOperation) {
  switch (mathOperation) {
    case "+":
      return num1 + num2;
    case "*":
      return num1 * num2;
    case "/":
      return num1 / num2;
    case "-":
      return num1 - num2;
    default:
      prompt("Enter mathematical operation");
  }
}
result = mathResult(num1, num2, mathOperation);
alert(`Result mathematical operation ${result}`);
